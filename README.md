# urd.opendatacity.de

This reopository contains configuration and documentation for urd.opendatacity.de, a server that contains experimental and legacy projects 
aimed to replace nyx.opendatacity.de.

It is named after [Urd](https://en.wikipedia.org/wiki/Ur%C3%B0r), the deity that represents the past in nordic mythology.
